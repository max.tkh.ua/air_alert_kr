import './Chart.scss';
import {getAlertWrapPosition, getBeautyHour, getDateTitle, getFullNum, getTimeDiff} from "../../services";
import {useSelector} from "react-redux";
import {IAlertSchedule} from "../../interfaces";

//TODO style by colors range
//TODO separate alert for two days

function Chart() {
    const storeData = useSelector((state:any) => state.counter)
    const calendarArray: number[] = storeData.calendarArray
    const alertSchedule: IAlertSchedule = storeData.alertsSchedule

    return (<>
        <div className="calendar">
            {calendarArray.map((coll: number) =>
                <div key={coll}>
                    <p className="calendar__day-title">{getDateTitle(coll)}</p>

                    <div className="day-coll">
                        {
                            alertSchedule[coll].map((alertDuration, index) => {
                                const startTime = new Date(alertDuration.startTime)
                                const endTime = new Date(alertDuration.endTime)
                                const duration = getTimeDiff(startTime, endTime)
                                return (
                                    <span
                                        key={index}
                                        className="air-alert"
                                        style={{
                                            top: getAlertWrapPosition(startTime.getHours(), startTime.getMinutes()),
                                            height: getAlertWrapPosition(endTime.getHours(), endTime.getMinutes()) - getAlertWrapPosition(startTime.getHours(), startTime.getMinutes())
                                        }}
                                    >
									<span className="popup">
										Ait Alert was <br/>
										started at <strong>{`${getFullNum(startTime.getHours())}:${getFullNum(startTime.getMinutes())}`}</strong>
										<br/>
										finished at <strong>{`${getFullNum(endTime.getHours())}:${getFullNum(endTime.getMinutes())}`}</strong>
										<br/>
										duration: <strong>{duration} min</strong>
                                        {duration > 60 && <strong> {getBeautyHour(duration)}</strong>}
									</span>
								</span>
                                )
                            })
                        }
                    </div>
                </div>
            )}
        </div>
    </>)
}

export default Chart;
