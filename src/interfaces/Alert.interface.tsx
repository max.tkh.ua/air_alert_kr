export interface IAlertSchedule {
    [key: string]: IAlertItem[]
}

export interface IAlertItem {
    startTime: string
    endTime: string
}
