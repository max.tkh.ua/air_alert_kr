import {IAlertSchedule as IAlertSchedule} from './Alert.interface';
import {IAlertItem as IAlertItem} from './Alert.interface';

export type {
    IAlertSchedule,
    IAlertItem
}
