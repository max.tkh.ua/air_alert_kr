import './app.scss';
import {Chart, Statistic} from "./components";

declare global {
    interface Date {
        addDays (days: number) : Date
    }
}

Date.prototype.addDays = function(days) {
    let date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

function App() {
    return (
        <main className="app">
            <Chart/>
            <Statistic/>
        </main>
    );
}

export default App;
