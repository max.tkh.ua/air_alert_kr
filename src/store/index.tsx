import {combineReducers, configureStore} from '@reduxjs/toolkit'
import alertsReducer from './alertsReducer'

const reducer = combineReducers({
    alerts: alertsReducer
});

const store = configureStore({
    reducer,
});

export type RootState = ReturnType<typeof reducer>;

export default store;
