import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {airAlertDB} from "../mockups";
import {IAlertItem, IAlertSchedule} from "../interfaces";
import {getTimeDiff, getTSDayStart} from "../services";

interface IInitialState {
    warDaysAmount: number,
    calendarArray: number[],
    alertsSchedule: IAlertSchedule
}

const initialState: IInitialState = {
    warDaysAmount: 0,
    calendarArray: [],
    alertsSchedule: {}
}

export const slice = createSlice({
    name: 'counter',
    initialState,
    reducers: {
        setSchedule: (state, action: PayloadAction<IInitialState>) => {
            state.warDaysAmount = action.payload.warDaysAmount
        }
    }
})

export default slice.reducer

const {setSchedule} = slice.actions

export const updateApplication = () => async (dispatch: any) => {
    const TSFirstDay = new Date('2022-02-24T04:54'); // 05:07
    const TSToday = new Date();
    const warDaysAmount = getTimeDiff(TSFirstDay, TSToday, 'days')
    let calendarArray = []
    let alertsSchedule:IAlertSchedule = {}
    let i = 0;

    while (i < warDaysAmount) {
        const curDay = new Date(getTSDayStart(TSFirstDay)).addDays(i).getTime()
        calendarArray.push(curDay)

        const alertInCurrentDay = airAlertDB.filter((item: IAlertItem) => getTSDayStart(item.startTime) === curDay)
        const alertDuringTwoDays = alertInCurrentDay.filter((item: IAlertItem) => getTSDayStart(item.endTime) !== curDay)

        if (alertDuringTwoDays.length > 0) {
            const nextDay = new Date(getTSDayStart(TSFirstDay)).addDays(i + 1).getTime()

            alertsSchedule[nextDay] = alertDuringTwoDays
        }

        alertsSchedule[curDay] = alertInCurrentDay

        i++
    }

    let data = {
        warDaysAmount,
        calendarArray,
        alertsSchedule,
    }

    dispatch(setSchedule(data));
};
