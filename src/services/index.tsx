const hourInPx = 20

const getTimeDiff = (firstDay:string | Date, lastDay:string | Date, type: string = 'minutes'): number => {
    const objFirstDay = firstDay instanceof Date ? firstDay : new Date(firstDay)
    const objLastDay = lastDay instanceof Date ? lastDay : new Date(lastDay)
    let ms = 1000

    switch (type){
        case 'hours':
            ms = ms * 3600;
            break;
        case 'days':
            ms = ms * 3600 * 24;
            break;
        default:
            ms = ms * 60;
    }

    return Math.ceil((objLastDay.getTime() - objFirstDay.getTime()) / ms)
}

const getTSDayStart = (date: string | Date): number => {
    const objDate = date instanceof Date ? date : new Date(date)

    return objDate.setHours(0, 0, 0, 0)
}

const getDateTitle = (date: number | Date): string => {
    const objDate = date instanceof Date ? date : new Date(date)
    const day = objDate.getDate()
    const month = objDate.getMonth() + 1

    return `${getFullNum(day)}.${getFullNum(month)}`
}

const getFullNum = (num: number) => {
    return num < 10 ? `0${num}` : num
}

const getAlertWrapPosition = (hour: number, minutes: number): number => {
    return hour * hourInPx + (minutes * hourInPx / 60)
}

const getBeautyHour = (duration: number): string => {
    const hours = Math.floor(duration / 60)
    const minutes = duration % 60

    return `(${getFullNum(hours)}:${getFullNum(minutes)})`
}

export {getTimeDiff, getTSDayStart, getDateTitle, getFullNum, getAlertWrapPosition, getBeautyHour}
